using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonTrigger : MonoBehaviour
{
    public bool triggered = false;
    private bool animating;
    private GameObject button;
    private Vector3 startPos;
    private float t = 0;
    private bool reset = true;

    private void Start()
    {
        button = this.transform.Find("button2").gameObject;
        startPos = button.transform.localPosition;
    }
    private void Update()
    {
        if (animating)
        {
            t += (Time.deltaTime) * 1f;
            button.transform.localPosition = Vector3.Lerp(startPos, new Vector3(0f, -0.15f, 0f), t);
            if (t >= 1)
            {
                animating = false;
                t = 0;
                Invoke("SetReset", 2f);
            }
        }
        if (!reset)
        {
            t += (Time.deltaTime) * 3f;
            button.transform.localPosition = Vector3.Lerp(new Vector3(0f, -0.15f, 0f), startPos, t);
            if (t >= 1)
            {
                reset = true;
                t = 0;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Hand_Left" || other.name == "Hand_Right")
        {
            if (!triggered && reset)
            {
                triggered = true;
                animating = true;
                return;
            }
            if (triggered && reset)
            {
                triggered = false;
                animating = true;
                return;
            }
        }
    }
    void SetReset()
    {
        reset = false;
    }
}
