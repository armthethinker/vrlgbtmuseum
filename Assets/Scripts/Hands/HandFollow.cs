using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(Rigidbody))]
public class HandFollow : MonoBehaviour {
    [SerializeField] 
    private bool _realPhysics = false;
    [SerializeField] 
    private float _followSpeed = 20f;
    [SerializeField] 
    private float _rotateSpeed = 100f;
    [SerializeField] 
    private Transform _followTarget;

    private Rigidbody _rigidbody;

    
    
    // Start is called before the first frame update
    void Start() {
        _rigidbody = GetComponent<Rigidbody>();
        // Reset hands
        transform.position = _followTarget.position;
        transform.rotation = _followTarget.rotation;
        if (_realPhysics) {
            _rigidbody.isKinematic = false;
        }
        else {
            _rigidbody.isKinematic = true;
        }
    }

    // Update is called once per frame
    void Update() {
        if (!_realPhysics) {
            SimpleMove();
        }
    }

    void FixedUpdate() {
        if (_realPhysics) {
            RealPhysicsMove();
        }
    }

    
    private void RealPhysicsMove() {
        // Position 
        float distance = Vector3.Distance(_followTarget.position, transform.position);
        Vector3 newVelocity = (_followTarget.position - transform.position).normalized * (_followSpeed *
                                                                                          distance);
        _rigidbody.AddForce(newVelocity - _rigidbody.velocity, ForceMode.VelocityChange);
        
        
        // Rotation
        Quaternion q = _followTarget.rotation * Quaternion.Inverse(_rigidbody.rotation);
        q.ToAngleAxis(out float angle, out Vector3 axis);
        Vector3 newAngularVelocity = (axis * (angle * Mathf.Deg2Rad * _rotateSpeed));
        _rigidbody.AddTorque(newAngularVelocity - _rigidbody.angularVelocity);
    }
    
    private void SimpleMove() {
        // smooth move
        transform.position = Vector3.Lerp(transform.position, _followTarget.position, Time.deltaTime * _followSpeed);
        transform.rotation = Quaternion.Lerp(transform.rotation, _followTarget.rotation, Time.deltaTime * _rotateSpeed);
    }
}
