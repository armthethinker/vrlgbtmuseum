using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    float RotationSpeed = 5f;

    void Update()
    {
        transform.Rotate(Vector3.up * (RotationSpeed * Time.deltaTime), Space.World);
    }
    
}
